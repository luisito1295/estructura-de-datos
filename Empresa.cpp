/*
	ActividadIntegradora
	Julio C�sar Villa Cervantes
	30/10/19 20:38
	Implementaci�n de las diferentes estructuras de control al lenguaje de C
*/

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<iostream>
using namespace std;

int main()
{
	int op;
	int joven,maduro,mayor;
	char razon,dom,rfc,tel,hr,num;
	op=1;

	
	do
	{
		printf("Implementaci�n de las diferentes estructuras de control al lenguaje de C'\n");
		printf("Elige una opcion del siguiente men� (1-4)\n");
		printf("1 Capturar datos de la empresa\n");
		printf("2 Imprime datos de la empresa\n");
		printf("3 Capturar las edades de los empleados\n");
		printf("4 Salir\n");
		printf("Elija una opcion (1-4):\n");
		scanf("%d", &op);
		
		switch(op)
		{	
			case 1: 
				printf("1 Capturar datos de la empresa\n");
				printf("A continuaci�n vamos a capturar los siguientes datos\n\n");
				printf("Razon Social\n");
				scanf("%s", &razon);
				printf("Domicilio Fiscal\n");
				scanf("%s", &dom);
				printf("Registro Federal de Contribuyentes R.F.C. \n");
				scanf("%s", &rfc);
				printf("Telefono \n");
				scanf("%s", &tel);
				printf("Horario (HH-HH)\n");
				scanf("%s", &hr);
			break;
			
			case 2:
				printf("2 Imprime datos de la empresa \n");
				printf("%s", razon);
				printf("%s", dom);
				printf("%s", rfc);
				printf("%s", tel);
				printf("%s", hr);
			break;
		
			case 3:
				int num,numempleado,i,n;
				maduro=0;
				printf("3 Capturar las edades de los empleados\n\n");
				printf("Cuantos empleados vamos a capturar?\n");
				scanf("%d", &numempleado);
				do
				{
					printf("Ingresa la edad del empleado\n");
					scanf("%d", &num);
					numempleado=numempleado-1;
					
						if ((num>0) && (num<22))
							{
								printf("es un empleado joven\n");
								joven=joven+1;
							}
								else 
									{
										if ((num>21) && (num<51))
										{
											printf("es un empleado maduro\n");
											maduro=maduro+1;
										}
									}
											if (num>50)
												{
													printf("es un empleado mayor\n");
													mayor=mayor+1;
												}	
										
												if (num<0)
												{
												printf("fuera de rango, se leer� otra edad\n");
												}
				}
				while(numempleado>0);
				printf ("Empleados jovenes: %d\n", joven);
				printf ("Empleados maduros: %d\n", maduro);
				printf ("Empleados mayores: %d\n", mayor);
			break;
			
			case 4:
				printf("Realmente desea salir? (1-SI, 2-NO)\n");
				scanf("%d",&op);
					if(op==1)
					{
						//op=5;
						printf("Fin del proceso");
						exit(-1);
					}
			break;
			
			default:
				printf("\nERROR!!!\n");
				printf("Seleccione una opcion valida como se muestra en el menu\n\n\n");
				
			
		}
	}
	while (op!=4 || op==4);
	
	getch();	
}
