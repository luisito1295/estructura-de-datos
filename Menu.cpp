#include<iostream>
#include<conio.h>
using namespace std;
#define largo 50
#define TAM 6
#define MAX_SIZE_VISIBLE 100

/*******************************************************************************************************
*****************Autor: Julio C�sar Villa Cervantes, C�digo: 218298309.*********************************
*****************Programa: Actividad Integradora: Implementacion de los diferentes algoritmos **********
*************************************para ordenamiento y busqueda***************************************
*/

/*funciones para ordenamiento por seleccion y mostrarlos*/
void leeCadena(int cant,int n[])
{
    int i;
    for(i=0;i<cant;i++)
    {
        cout<<"Ingresa numero "<<i+1<<": ";
        cin>>n[i];
    }
     
}
 
void muestraCadena(int cant,int n[])
{
    int i;
    for(i=0;i<cant;i++)
    {
        cout<<n[i]<<endl;
    }
}

void seleccionsort (int  A[], int n) 
{
    int min,i,j,aux;
    for (i=0; i<n-1; i++) 
    {
          min=i;
          for(j=i+1; j<n; j++)
                if(A[min] > A[j])
                   min=j;
          aux=A[min];
          A[min]=A[i];
          A[i]=aux ;
    }

} 
/*Fin*/

/*Metodo de ordenamiento Quiksort*/
void intercambio(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

/*funcion quicksort para ordenar el arreglo*/
void quicksort(int* izq, int* der) {
    if (der < izq)
        return;
    int pivote = *izq;
    int* ult = der;
    int* pri = izq;
    while (izq < der) {
        while (*izq <= pivote && izq < der+1)
            izq++;
        while (*der > pivote && der > izq-1)
            der--;
        if (izq < der)
            intercambio(izq, der);
    }
    intercambio(pri, der);
    quicksort(pri, der-1);
    quicksort(der+1, ult);
}
/*Fin*/

int A[50],n;
int main() 
{
	
	int Opcion,i;
	int numeros,n;
	int aux,j,max;
	

	printf ("\n Actividad Integradora: Implementacion de los diferentes algoritmos para ordenamiento y busqueda\n");
	printf ("Ingresa 1 ordenamiento burbuja.\n");
	printf ("Ingresa 2 ordenamiento insercion.\n");
	printf ("Ingresa 3 ordenamiento seleccion.\n");
	printf ("Ingresa 4 ordenamiento quickSort.\n");
	printf ("Ingresa 5 busqueda secuencial.\n");
	printf ("Ingresa 6 busqueda binaria.\n");
	printf ("Ingresa 7 para salir.\n");
	scanf ("%d",&Opcion);
	
	if (Opcion==1)
	{
	
		printf("Opcion 1:Modelo de ordenamiento Burbuja\n");
 		
 		cout<<"cuantos elementos quiere usar: ";
		cin>>n;
		int numeros[n];
		
		
		for(int i=0;i<n;i++)
		{
			cout<<"Digite un numero: ";
			cin>>numeros[i];
		}
		
		for(int i=0;i<n;i++)
		{
			cout<<i<<" -> "<<numeros[i]<<endl; 
		}
		
		for(int i = 0; i <n; i++)
		{
			for(int j = 0; j < n; j++)
			{
			
				if (numeros[j] > numeros[j+1])
				{
					aux = numeros[j+1];
					numeros[j+1] = numeros[j];
					numeros[j] = aux;
				}
			}
		}
		
		for(int i = 1; i < n+1; i++)
		{
			printf("%d\t \n", numeros[i]);
		}
			
		return 0;
	
	}


	if (Opcion==2)
	{
		int max[n];
		int i,j,aux;
		printf("Opcion 2:Modelo de ordenamiento insercion\n");
		
		cout<<"cuantos elementos quiere usar: ";
		cin>>n;
		int numeros[n];
		
		
		for(int i=0;i<n;i++)
		{
			cout<<"Digite un numero: ";
			cin>>numeros[i];
		}
		
		for(int i=0;i<n;i++)
		{
			cout<<i<<" -> "<<numeros[i]<<endl; 
		}
		for(int i=0; i< n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				if (max[j] > max[j+1])
				{
					aux = max[j+1];
					max[j+1] = max[j];
					max[j] = aux;
				}
				
			}
		}
		for(int i = 1; i < n+1; i++)
		{
			printf("%d\t \n", max[i]);
			system ("pause");
		}			
    }
    if(Opcion==3){
    	printf("Opcion :Modelo de ordenamiento por seleccion\n");
	    int A[largo],n;
	    do{
	    cout<<"Cantidad de numeros a ingresar: ";cin>>n;
	        if(n<=0||n>largo)
	            cout<<"Debe ingresar un valor  > a 0 y < a "<<largo<<endl;
	    }while(n<=0||n>largo);
	    
	    leeCadena(n,A);
    	seleccionsort(A,n);
		muestraCadena(n,A);
	}
	
	if(Opcion==4){
		printf("Opcion 4:Modelo de ordenamiento Quicksort\n");
		int i;
	    int tam;
	
	    /*definimos el tama�o del arreglo*/
	    printf("Ingrese el tama�o del arreglo:\n");
	    scanf("%d", &tam);
	    int arreglo[tam];
	
	    /*llenamos el arreglo*/
	    printf("Ingrese valores para el arreglo:\n");
	    for (i = 0; i < tam; i++)
	        scanf("%d", &arreglo[i]);
	    printf("n");
	
	    /*mostramos el arreglo original*/
	    printf("Arreglo Original\n");
	    for (i = 0; i < tam; i++)
	        printf("%d ", arreglo[i]);
	    printf("nn");
	
	    /*hacemos el llamado a la funcion quicksort
	      para que ordene el arreglo*/
	    quicksort(&arreglo[0], &arreglo[tam-1]);
	
	    /*mostramos el arreglo ordenado*/
	    printf("Arreglo Ordenado\n");
	    for (i = 0; i < tam; i++)
	        printf("%d ", arreglo[i]);
	    printf("\n\n");
		
	}
	
	if(Opcion==5){
		
		printf("Opcion 5:Modelo de busqueda secuencial \n");
		
		int clave,encontrado, n, i, j;
		printf("Ingrese dimensi�n del arreglo: ");
		scanf("%d",&n);
		int* arreglo =new int[n];//LLenar el arreglo. 
		for (i = 0;i < n;i++){
			printf("Ingrese dato %d [",i,"]");
			scanf("%d",&arreglo[i]);
		}
		
		//Pedir dato o clave a buscar. 
		printf("Ingrese dato que desea buscar: ");
		scanf("%d",&clave);
		encontrado = 0;//Buscar clave o dato en el arreglo. 
		for (j = 0;j < n;j++){//Si el elemento de la posici�n actual del arreglo es similar a la clave de b�squeda, despliega mensaje de encontrado. 
			if (arreglo[j] == clave){
				printf("Se encontro el %d",clave," en laposicion %d [",j,"] \n");
				encontrado = 1;
			}
		}
		
		delete[] arreglo;//Si no se encontr� la clave despliega el mensaje de noencontrado. 
		if (encontrado != 1) printf("No se encontro el dato");
		system("pause");
		return 0;
	}
	
	if(Opcion==6){
		printf("Opcion 6:Modelo de busqueda binaria \n");
		int medio, inicio, fin, valormedio, clave, n, encontrado;
		int temp, i, j, m, p;inicio = 0;
		printf("Ingrese dimension del arreglo: ");
		scanf("%d",&n);
		fin = n - 1;
		int* arreglo =new int[n];
		for (i = 0;i < n;i++){
			printf("Ingrese dato %d ",i);
			scanf("%d",&arreglo[i]);
		}
		temp = 0;j= 0;m = 0;p = 0;
		for (j = 1; j < n; j++){
			for (m = 0; m < n-1; m++){
				if(arreglo[m]>arreglo[m + 1]){
					temp = arreglo[m + 1];
					arreglo[m + 1] = arreglo[m];arreglo[m] = temp;
					}
				}
			}//Mostrar arreglo ordenado.
			printf("El arreglo Ordenado es :\n");
			for (p = 0; p <= n - 1; p++){
				cout<< "Posicion [" << p<< "] = " << arreglo[p]<< endl;
			}//Pedir dato o clave a buscar. 
			printf("Ingrese dato que desea buscar: ");
			scanf("%d",&clave);//Buscar clave o dato deseado. 
			encontrado = 0;
			while (inicio <= fin){
			medio = (fin + inicio) / 2;
			if (arreglo[medio] == clave){
				printf("Se encontro el %d",clave, " en laposicion %d [",medio,"] \n");
				encontrado = 1;
				fin = -1;
			}
			else
				if (clave < arreglo[medio]){
					fin = medio - 1;
				}
				else {
					inicio = medio + 1;
				}
			}
			if (encontrado != 1) { 
				printf("No se encontro el dato");
			}
			delete[] arreglo;
			system("pause");
			return 0;
	}
	
    getch();
	return 0;
}

